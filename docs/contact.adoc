= Contacting Us

We primarly use email and IRC to communicate. Please note the link:https://robigalia.org/conduct.html[code of conduct], which you'll be expected to follow in our spaces.

[[mailing-list]]
== Mailing List

We have a mailing list link:https://lists.robigalia.org/listinfo/robigalia-dev[here], which has updates from the devs and occasional discussion.

[[irc]]
== IRC

We have an http://webchat.freenode.net/?channels=%23robigalia[IRC channel] on Freenode, `#robigalia`, for more real-time communication. In practice, this is definitely where most communications takes place.

== Twitter

There's a https://twitter.com/robigalia_proj[twitter account] where new blog posts get posted. Feel free to tweet at us!
