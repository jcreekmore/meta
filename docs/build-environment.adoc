[[setting-up-a-build-environment]]
Setting up a build environment
------------------------------

Setting up a build environment for Robigalia is easy! First, clone the "devbox" repository:

    git clone --recursive git@gitlab.com:robigalia/devbox.git robigalia

Set the `RUST_TARGET_PATH` environment variable in your shell configuration
to `path/to/robigalia/sel4-targets`. Make sure you have a recent Rust nightly installed. 
If you're using `rustup`, run something like:

    rustup default nightly-$DATE

The `$DATE` we use for CI can be found https://gitlab.com/robigalia/runner/blob/master/Dockerfile#L33[approximately here].

After that, just one last step for Rust:

    cargo install xargo

There are also some Python-related dependencies that need to be installed. If you have `pip` available, the following should be sufficient:

    pip install --user tempita ply

You will also need to have `clang` installed, with the ability to target the architectures you plan on building for. This is currently used to compile some of the assembly files we use. In the future, we hope to remove this dependency.

Now, you should be able to `cd path/to/robigalia/hello-world` and execute:

    xargo build --target x86_64-sel4-robigalia

To build the hello-world project.

If you wish, you can also use the https://gitlab.com/robigalia/runner/tree/master[Docker container] that we use for CI. However, this hasn't been tested outside of the context of CI, and I don't know how to use it productively.

[[troubleshooting]]
Troubleshooting
---------------

If you tried following these steps but came across an error, *please* come
to our https://robigalia.org/contact.html[IRC channel or mailing list] so we
can help you, and add more details to this section. Thanks!
